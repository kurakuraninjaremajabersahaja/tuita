# Nama Kelompok

	Kura-Kura Ninja Remaja

# Anggota Kelompok

1. Muhammad Yusuf Sholeh / 1606862791
2. Nabilla Hariyana / 1606833362
3. Priscilla Tiffany / 1606822964
4. Syafiq Abdillah U. / 1606890574

# Links

  - Nama Heroku 	: https://tuitaa.herokuapp.com/
  - Nama GitLab		: https://gitlab.com/kurakuraninjaremajabersahaja/tuita.git

# Coverage and Pipelines

[![pipeline status](https://gitlab.com/kurakuraninjaremajabersahaja/tuita/badges/master/pipeline.svg)](https://gitlab.com/kurakuraninjaremajabersahaja/tuita/commits/master)

[![coverage report](https://gitlab.com/kurakuraninjaremajabersahaja/tuita/badges/master/coverage.svg)](https://gitlab.com/kurakuraninjaremajabersahaja/tuita/commits/master)