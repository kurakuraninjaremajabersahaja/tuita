from django.test import TestCase
from django.test import Client
from django.utils import timezone
from unittest import skip
from app_update.models import Status
from app_addFriend.models import Friends

# Create your tests here.
class UnitTest(TestCase):
	def test_stats_is_exist(self):
		response = Client().get('/stats/')
		self.assertEqual(response.status_code, 200)

	def test_stats_has_status_count(self):
		status_dummy = Status.objects.create(isi='dummy status')

		response = Client().get('/stats/')
		html_response = response.content.decode('utf8')

		self.assertIn('<strong>Feed</strong> 1 Post', html_response)

	def test_stats_has_friend_count(self):
		friend_dummy = Friends.objects.create(name='dummy friend', url='http://dummyurl.friend/')

		response = Client().get('/stats/')
		html_response = response.content.decode('utf8')

		self.assertIn('<strong>Friends</strong> 1 People', html_response)

	def test_stats_has_latest_post(self):
		status_dummy = Status.objects.create(isi='dummy status')

		response = Client().get('/stats/')
		html_response = response.content.decode('utf8')

		self.assertIn(status_dummy.isi, html_response)

	def test_stats_has_no_latest_post(self):
		no_post_text = "<strong>Oops!</strong> Takde status."

		response = Client().get('/stats/')
		html_response = response.content.decode('utf8')

		self.assertIn(no_post_text, html_response)