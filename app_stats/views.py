from django.shortcuts import render
from app_addFriend.models import Friends
from app_update.models import Status

img_url = "https://pbs.twimg.com/media/DLzIgLLUIAATlZY.png:large"
response = {}

def index(request):
    jumlah_teman = Friends.objects.all().count()
    jumlah_stats = Status.objects.all().count()
    latest_stats = Status.objects.first()
    response['jumlah_teman'] = jumlah_teman
    response['jumlah_stats'] = jumlah_stats
    response['latest_stats'] = latest_stats
    response['img_url'] = img_url
    return render(request, 'stats.html', response)

    
