"""twita URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import app_update.urls as index_update
import app_profile.urls as index_profile
import app_addFriend.urls as index_addfriend
import app_stats.urls as index_stats
from django.views.generic.base import RedirectView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/update/welcome_page', permanent=False), name='index'),
    url(r'update/', include(index_update, namespace='update')),
    url(r'profile/', include(index_profile, namespace='profile')),
    url(r'add/', include(index_addfriend, namespace='add')),
    url(r'stats/', include(index_stats, namespace='stats')),
]
