from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Biodata

response = {}
def index(request):
    profile = Biodata.objects.get(pk=1)
    split_expertise = profile.expertise.split(',')
    response['profile'] = profile
    response['split_expertise'] = split_expertise
    return render(request, 'profile.html', response)
