from django.db import models

class Biodata(models.Model):
    name = models.CharField(max_length=27)
    email = models.EmailField()
    birthdate = models.DateField()
    gender = models.CharField(max_length=6)
    expertise = models.TextField()
    description = models.TextField()
