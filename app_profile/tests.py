from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Biodata

def user_profile():
    return Biodata.objects.create(name="Kak Pewe", email='kakpewe@ppw.com',
        birthdate='2012-04-04', gender="Male", expertise="mengajar",
        description="Halo semua, kalau bingung boleh tanya saya")

class ProfileUnitTest(TestCase):
     def test_profile_url_is_exist(self):
         response = Client().get('/profile')
         self.assertEqual(response.status_code,301)

     def test_profile_using_index_func(self):
         found = resolve('/profile/')
         self.assertEqual(found.func, index)

     def test_model_can_create_new_user(self):
         user = user_profile()
         counting_all_user = Biodata.objects.all().count()
         self.assertEqual(counting_all_user, 1)

     def test_user_profile_has_data(self):
        user = user_profile()
        response= Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(user.name, html_response)
        self.assertIn(user.email, html_response)
        self.assertIn(user.gender, html_response)
        self.assertIn(user.expertise, html_response)
        self.assertIn(user.description, html_response)
