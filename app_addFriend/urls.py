from django.conf.urls import url
from .views import index, add_friend, unfriend

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_friend', add_friend, name='add_friend'),
    url(r'^unfriend/(?P<id>\d+)/$', unfriend, name='unfriend'),
]