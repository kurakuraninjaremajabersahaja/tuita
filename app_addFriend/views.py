from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django import forms
from .forms import Add_Friend_Form
from .models import Friends
from django.contrib import messages

response = {}
def index(request):
    allFriends = Friends.objects.all()
    response['allFriends'] = allFriends
    response['add_friend_form'] = Add_Friend_Form
    return render(request, 'friend.html', response)

def add_friend(request):
    form = Add_Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        if validate_url(request.POST['url']):
            response['url'] = request.POST['url']
            friend = Friends(name=response['name'],url=response['url'])
            friend.save()
            messages.success(request, 'Welcome your new friend!')
        else:
            messages.warning(request, 'Are you sure that is your friend\'s URL? Try again!')
        return HttpResponseRedirect('/add/')
    else:
        messages.warning(request, 'Are you sure that is your friend\'s URL? Try again!')
        return HttpResponseRedirect('/add/')

def validate_url(url):
    validate = URLValidator()
    try:
        validate(url)
    except ValidationError:
        return False
    return True

def unfriend(request,id):
    friend = Friends.objects.get(id=id)
    friend.delete()
    return HttpResponseRedirect('/add/')