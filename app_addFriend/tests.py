from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_friend, unfriend
from .models import Friends
from .forms import Add_Friend_Form

class AddFriendUnitTest(TestCase):

    def test_add_friend_url_is_exist(self):
        response = Client().get('/add/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_friend(self):
        new_friend = Friends.objects.create(name='Teman baikku', url='https://www.joox.com/')
        counting_all_friend = Friends.objects.all().count()
        self.assertEqual(counting_all_friend, 1)

    def test_add_friend_post_success_and_render_the_result(self):
        name = 'Bestieku'
        url = 'http://hehe.com'
        response_post = Client().post('/add/add_friend', {'name': name, 'url': url})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add/')
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)
        self.assertIn(url, html_response)

    def test_add_friend_link_invalid_and_render_the_result(self):
        name = 'Bestieku'
        url = 'hehe.com'
        response_post = Client().post('/add/add_friend', {'name': name, 'url': url})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(name, html_response)
        self.assertNotIn(url, html_response)

    def test_add_friend_post_error_and_render_the_result(self):
        name = 'Bestieku'
        url = 'http://hehe.com'
        response_post = Client().post('/add/add_friend', {'name': '', 'url': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(name, html_response)
        self.assertNotIn(url, html_response)

    def test_form_validation_for_blank_items(self):
        name = 'Bestieku'
        url = 'hehe.com'
        form = Add_Friend_Form(data={'name': '', 'url': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['name'],["This field is required."])

    def test_model_can_delete_friend(self):
        new_activity = Friends.objects.create(name='Bestieku', url='http://tuitaa.herokuapp.com')
        counting_all_friend = Friends.objects.all().count()

        response= Client().get('/add/')
        html_response = response.content.decode('utf8')

        unfriend(html_response,Friends.objects.get(name='Bestieku').id)
        
        counting_after_unfriend = Friends.objects.all().count()

        self.assertEqual(counting_all_friend-1,counting_after_unfriend)