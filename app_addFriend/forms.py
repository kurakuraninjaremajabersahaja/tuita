from django import forms

class Add_Friend_Form(forms.Form):
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'What\'s your friend\'s name?',
        'size':140
    }
    url_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'What\'s your friend\'s URL?',
        'size':140
    }

    name = forms.CharField(label='Name', required=True, max_length=100, widget=forms.TextInput(attrs=name_attrs))
    url = forms.URLField(label='URL', required=True, widget=forms.TextInput(attrs=url_attrs))