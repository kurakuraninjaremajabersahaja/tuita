from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
#from app_profile.views import img_url
img_url = "https://pbs.twimg.com/media/DLzIgLLUIAATlZY.png:large"

# Create your views here.
response = {}
def welcome_page(request):
    return render(request,'welcome_page.html',response)

def index(request):
    response['author'] = "Helvetica Standard"
    status = Status.objects.all()
    response['status'] = status
    response['img_url'] = img_url
    html = 'update.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == "POST" and form.is_valid()):
        response['isi'] = request.POST['isi']
        status = Status(isi = response['isi'])
        status.save()
        return HttpResponseRedirect('/update/')
    else:
        return HttpResponseRedirect('/update/')

def delete_status(request,id):
    status = Status.objects.get(id=id)
    status.delete()
    return HttpResponseRedirect('/update/')