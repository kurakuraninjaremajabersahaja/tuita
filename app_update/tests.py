from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, welcome_page, delete_status
from .models import Status
from .forms import Status_Form

# Create your tests here.
class App_updatestatusUnitTest(TestCase):

    def test_welcome_page_url_is_exist(self):
        response = Client().get('/update/welcome_page')
        self.assertEqual(response.status_code, 200)

    def test_welcome_page_using_welcome_page_func(self):
        found = resolve('/update/welcome_page')
        self.assertEqual(found.func, welcome_page)

    def test_app_updatestatus_url_is_exist(self):
        response = Client().get('/update/')
        self.assertEqual(response.status_code, 200)

    def test_app_updatestatus_using_index_func(self):
        found = resolve('/update/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new status
        new_status = Status.objects.create(isi='ini adalah tes status')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)      

    def test_app_update_has_no_post(self):
        no_post_text = "<strong>Oops!</strong> Takde status."

        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')

        self.assertIn(no_post_text, html_response)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'isi': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['isi'],
            ["This field is required."]
        )

    def test_app_updatestatus_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update/add_status', {'isi': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_app_updatestatus_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update/add_status', {'isi': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_model_can_delete_status(self):
        new_activity = Status.objects.create(isi='Semoga Test ini berhasil')
        counting_all_status = Status.objects.all().count()

        response= Client().get('/update/')
        html_response = response.content.decode('utf8')

        delete_status(html_response,Status.objects.get(isi='Semoga Test ini berhasil').id)
        
        counting_after_delete = Status.objects.all().count()

        self.assertEqual(counting_all_status-1,counting_after_delete)