from django.conf.urls import url
from .views import welcome_page, index, add_status, delete_status

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^welcome_page', welcome_page, name='welcome_page'),
    url(r'^delete_status/(?P<id>\d+)/$', delete_status, name='delete_status'),
]
