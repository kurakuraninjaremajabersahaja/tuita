from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong dong isi :c',
    }
    isi_attrs = {
        'type': 'text',
        'cols':155,
        'rows':6,
        'class': 'form-control',
        'placeholder':'What\'s on your mind ?',
        'style':'resize:none;'
    }

    isi = forms.CharField(label='', required=True, max_length=140, widget=forms.Textarea(attrs=isi_attrs))
    
